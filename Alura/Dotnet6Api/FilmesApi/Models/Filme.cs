using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FilmesApi.Models;

public class Filme
{
    public int Id { get; set; }
    [Required(ErrorMessage ="O título do filme é obrigatório")]
    public string Titulo { get; set; }
    [Required(ErrorMessage ="O título do gênero é obrigatório")]
    [MaxLength(50, ErrorMessage ="O tamanho do gênero não pode exceder 50 caractetes")]
    public string Genero { get; set; }
    [Required]
    [Range(70, 600, ErrorMessage ="A duraçao deve ter entre 70 e 600 minutos")]
    public int Duracao { get; set; }
}
