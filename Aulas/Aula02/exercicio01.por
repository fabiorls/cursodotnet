programa {
  funcao inicio() {
    inteiro numero1, numero2, resultado

    escreva("Digite o primeiro n�mero\n")
    leia(numero1)

    escreva("\nDigite o segundo n�mero\n")
    leia(numero2)

    resultado = numero1 + numero2

    escreva("\nA soma dos n�meros �: ", resultado, "\n")
  }
}
