programa {
  funcao inicio() {
    inteiro numero

    escreva("Digite o valor\n")
    leia(numero)
    
    se(numero % 2 == 0){
      escreva("\n O n�mero � par")
    } senao{
      escreva("\n O n�mero � impar")
    }
  }
}
