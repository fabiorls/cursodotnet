programa {
  funcao inicio() {
    inteiro numero

    escreva("Digite o valor\n")
    leia(numero)

    para(inteiro i = numero; i > 0; i--){
      se(numero % i == 0){
        escreva("\n", i, " � um divisor de ", numero, "\n")
      }
    }
  }
}
