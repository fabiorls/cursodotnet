programa {
  funcao inicio() {
    inteiro numero

    escreva("Digite o valor\n")
    leia(numero)

    escreva("A tabuada de ", numero, " �:")
    para(inteiro i = 1; i <= 10; i++){
      escreva("\n", numero*i)
    }
  }
}
