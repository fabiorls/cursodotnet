programa {
  funcao inicio() {
    inteiro matriz[3][3]

    escreva("Digite os valores da matriz\n")
    para(inteiro linha = 0; linha < 3; linha++){
      para(inteiro coluna = 0; coluna < 3; coluna++){
        leia(matriz[linha][coluna])
      }
    }

    escreva("A matriz �:\n")
    para(inteiro linha = 0; linha < 3; linha++){
      para(inteiro coluna = 0; coluna < 3; coluna++){
        escreva("[", matriz[linha][coluna], "] ")
      }
      escreva("\n")
    }

    escreva("A matriz transposta �:\n")
    para(inteiro linha = 0; linha < 3; linha++){
      para(inteiro coluna = 0; coluna < 3; coluna++){
        escreva("[", matriz[coluna][linha], "] ")
      }
      escreva("\n")
    }
  }
}
