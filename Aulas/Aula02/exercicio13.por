programa {
  funcao inicio() {
    inteiro numero, resultado = 1

    escreva("Digite um n�mero\n")
    leia(numero)

    para(inteiro i = numero; i > 0; i--){
        resultado = resultado * i
    }

    escreva("\nO Fatorial de ", numero, " �: ", resultado)
  }
}
