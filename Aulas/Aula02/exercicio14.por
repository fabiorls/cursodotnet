programa {
  funcao inicio() {
    inteiro numero, quantidadeDeDivisores = 1

    escreva("Digite o valor\n")
    leia(numero)

    para(inteiro i = numero; i > 1; i--){
      se(numero % i == 0){
        quantidadeDeDivisores++
      }
    }

    se(quantidadeDeDivisores > 2){
      escreva("\n", numero, " N�O � um n�mero primo")
    } senao {
      escreva("\n", numero, " � um n�mero primo")
    }
  }
}
