programa {
  funcao inicio() {
    inteiro numero1, numero2, resultado = 1, numero1Apresentar, numero2Apresentar

    escreva("Digite o primeiro valor\n")
    leia(numero1)

    escreva("Digite o segundo valor\n")
    leia(numero2)

    numero1Apresentar = numero1
    numero2Apresentar = numero2

    para(inteiro i = 2; i <= numero1 ou i <= numero2; i++){
      se(numero2 % i == 0 e numero1 % i == 0){
        resultado = resultado * i
        numero1 = numero1 / i
        numero2 = numero2 / i
        i--
      }
      senao se(numero1 % i == 0){
        resultado = resultado * i
        numero1 = numero1 / i
        i--
      }
      senao se(numero2 % i == 0){
        resultado = resultado * i
        numero2 = numero2 / i
        i--
      }
    }
    escreva("O MMC de ", numero1Apresentar, " e ", numero2Apresentar, " �: ", resultado)
  }
}

