programa {
  funcao inicio() {
    inteiro numero, primeiroAnterior = 1, segundoAnterior = 1, resultado = 1

    escreva("Digite o n�mero desejado da sequ�ncia de Fibonacci\n")
    leia(numero)

    para(inteiro i = 3; i <= numero; i++){
      resultado = primeiroAnterior + segundoAnterior
      primeiroAnterior = segundoAnterior
      segundoAnterior = resultado
    }

    escreva("O ", numero, "� termo da sequ�ncia de Fibonacci �: ", resultado)
  }
}


