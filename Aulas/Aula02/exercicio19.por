programa {
  funcao inicio() {
    inteiro numero, resultado

    escreva("Digite um n�mero\n")
    leia(numero)

    resultado = numero

    para(inteiro i = numero; i > 10; i = i/10){
      resultado = i/10
    }

    escreva("O d�gito mais significativo de ", numero, " �: ", resultado)
  }
}



