﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicio1Calculadora
{
    internal class Calculadora
    {
        public double PrimeiroValor {  get; private set; }
        public double SegundoValor { get; private set; }

        public Calculadora(double primeiroValor, double segundoValor)
        { 
            this.PrimeiroValor = primeiroValor;
            this.SegundoValor = segundoValor;
        }

        public virtual void SetPrimeiroValor(double primeiroValor)
        {
            this.PrimeiroValor = primeiroValor;
        }
        public virtual void SetSegundoValor(double segundoValor)
        {
            this.SegundoValor = segundoValor;
        }

        public double Adicao()
        {
            return PrimeiroValor + SegundoValor;
        }
        public double Subtracao()
        {
            return PrimeiroValor - SegundoValor;
        }
        public double Multiplicacao()
        {
            return PrimeiroValor * SegundoValor;
        }
        public double Divisao()
        {
            return PrimeiroValor / SegundoValor;
        }
    }
}
