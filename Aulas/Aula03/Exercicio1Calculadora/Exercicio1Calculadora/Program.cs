﻿using System.Reflection;
using Exercicio1Calculadora;

namespace aula03;

internal class Program
{
    static void Main(string[] args)
    {
        Calculadora calculo1 = new Calculadora(10.5, 8.9);
        Console.WriteLine(calculo1.Adicao());
        Console.WriteLine(calculo1.Subtracao());
        Console.WriteLine(calculo1.Multiplicacao());
        Console.WriteLine(calculo1.Divisao());
        calculo1.SetPrimeiroValor(55.8);
        Console.WriteLine(calculo1.Adicao());
    }
}