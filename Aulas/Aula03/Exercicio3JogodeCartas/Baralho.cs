using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercicio3JogodeCartas
{
    public class Baralho
    {
        public List<Carta> Cartas = new List<Carta>();
        public Baralho()
        {
            for(int j = 0; j < 2; j++)
            {
                for(int i = 2; i <= 10; i++)
                {
                    foreach(var value in Enum.GetValues(typeof(ENaipe)))
                        Cartas.Add(new Carta(i.ToString(), value.ToString()));
                }
                foreach(var value in Enum.GetValues(typeof(EValores)))
                    foreach(var value2 in Enum.GetValues(typeof(ENaipe)))
                        Cartas.Add(new Carta(value.ToString(), value2.ToString()));
            }
        }
        public void Embaralhar()
        {
            var random = new Random();
            for (int i = 0; i < Cartas.Count; i++)
            {
                var j = random.Next(Cartas.Count);
                var temp = Cartas[i];
                Cartas[i] = Cartas[j];
                Cartas[j] = temp;
            }

        }
    }
}