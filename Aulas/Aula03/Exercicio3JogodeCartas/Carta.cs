using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercicio3JogodeCartas
{
    public class Carta
    {
        public string Valor;
        public string Naipe;
        public Carta(string valor, string naipe)
        {
            Valor = valor;
            Naipe = naipe;
        }
    }
}