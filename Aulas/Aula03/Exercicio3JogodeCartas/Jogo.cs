using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercicio3JogodeCartas
{
    public class Jogo
    {
        public Baralho baralho = new Baralho();
        public List<Jogador> jogadores = new List<Jogador>();
        public Jogo(int numeroDeJogadores)
        {
            baralho.Embaralhar();
            for(int j = 0; j < numeroDeJogadores; j++)
            {
                var jogadorAuxiliar = new Jogador();
                for(int i = 0; i < 9; i++)
                {
                    jogadorAuxiliar.CartasDoJogador.Add(baralho.Cartas[0]);
                    baralho.Cartas.RemoveAt(0);
                }
                jogadores.Add(jogadorAuxiliar);
            }
        }
    }
}