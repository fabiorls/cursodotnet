﻿using System.Reflection;
using System.Collections.Generic;

namespace Exercicio3JogodeCartas;

internal class Program
{
    static void Main(string[] args)
    {
       // var carta = new Carta("ff", "ENaipe.Copas");
        //var baralho = new Baralho();
        //Console.WriteLine(carta.Naipe);
        //foreach(var value in Enum.GetValues(typeof(ENaipe)))
            //Console.WriteLine(value);

        //Console.WriteLine(Enum.GetName(typeof(ENaipe), 3));

        //List<Carta> Cartas = new List<Carta>();
        //var cartaTeste = new Carta("3", "Paus");
        //Cartas.Add(new Carta("3", "Paus"));
        // Cartas.Add(new Carta("5", "Ouro"));
        /*foreach(var value in baralho.Cartas)
            Console.WriteLine(value.Valor + " de " + value.Naipe);
        
        Console.WriteLine("Agora vamos embaralhar\n");
        baralho.Embaralhar();

        foreach(var value in baralho.Cartas)
            Console.WriteLine(value.Valor + " de " + value.Naipe);

        Console.WriteLine("\n" + baralho.Cartas.Count());*/
        var jogo = new Jogo(6);
        foreach(var value in jogo.baralho.Cartas)
            Console.WriteLine(value.Valor + " de " + value.Naipe);

        Console.WriteLine("\n" + jogo.baralho.Cartas.Count());
        Console.WriteLine("\n" + jogo.jogadores.Count());

        foreach(var value in jogo.jogadores.Select((item, index) => (item, index)))
        {
            Console.WriteLine($"Jogador {value.index}:");
            foreach(var value2 in value.item.CartasDoJogador)
                Console.WriteLine(value2.Valor + " de " + value2.Naipe);
        }
    }
}
