﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula._03
{
    internal class Animal : IAnimal
    {
        //public DateTime DataNascimento { get; private set; }
        public int Idade { get; set; }
        public string Especie { get; set; } = String.Empty;

        /*public Animal(DateTime dataNascimento, string especie)
        {
            this.DataNascimento = dataNascimento;
            this.Especie = especie;
        }

        public DateTime SetDataAniversario(DateTime dataNascimento)
        {
            return this.DataNascimento = dataNascimento;
        }

        public string DataNascimentoFormatada()
        {
            return this.DataNascimento.ToString("dd/MM/yyy");
        }*/

        public Animal(int idade, string especie) 
        {
            this.Idade = idade;
            this.Especie = especie;
        }

        public virtual void EmitirSom()
        {
            Console.WriteLine("Emitir som...");
        }

        public virtual void Andar()
        {
            Console.WriteLine("Andando...");
        }
    }
}
