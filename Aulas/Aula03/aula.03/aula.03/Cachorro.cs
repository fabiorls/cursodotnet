﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula._03
{
    internal class Cachorro : Animal
    {
        public Cachorro(int idade) : base(idade, "Chachorro")
        {
        }

        public virtual void Latir()
        {
            Console.WriteLine("Au au!");
        }

        public override void EmitirSom()
        {
            Console.WriteLine("Au au!");
        }
    }
}
