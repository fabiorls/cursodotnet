﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aula._03
{
    internal class Gato : Animal
    {
        public Gato(int idade) : base(idade, "Gato")
        {
        }

        public override void EmitirSom()
        {
            Console.WriteLine("Miau!");
        }
    }
}
