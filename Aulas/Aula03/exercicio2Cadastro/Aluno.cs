using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace exercicio2Cadastro
{
    public class Aluno
    {
        public string Nome {get; set;} = String.Empty;

        public int Idade {get; set;}
        public double Nota {get; private set;}

        public Aluno()
        {
        }

        public virtual void SetNota(double nota)
        {
            this.Nota = nota;
        }
    }
}