﻿using System.Reflection;
using exercicio2Cadastro;

namespace aula03;

internal class Program
{
    static void Main(string[] args)
    {
        Aluno fabio = new Aluno();
        fabio.SetNota(8.8);
        Aluno fabio2 = new Aluno();
        fabio2.SetNota(5.8);
        Aluno fabio3 = new Aluno();
        fabio3.SetNota(7.8);
        Turma turmaA = new Turma();
        turmaA.Alunos.Add(fabio);
        turmaA.Alunos.Add(fabio2);
        turmaA.Alunos.Add(fabio3);
        Console.WriteLine(turmaA.CalcularMedia());
    }
}