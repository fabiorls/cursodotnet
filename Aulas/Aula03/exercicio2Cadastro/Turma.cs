using exercicio2Cadastro;

public class Turma
{
    public List<Aluno> Alunos = new List<Aluno>();

    public Turma()
    {}
    public double CalcularMedia()
    {
        double total = 0;
        foreach (var notas in Alunos)
            total += notas.Nota;

        return total / Alunos.Count;

    }
}