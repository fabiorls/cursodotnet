﻿using FilaComPrioridade;

var cliente1 = new Cliente("Souza", 29);
var cliente2 = new Cliente("Ricardo", 23);
var cliente3 = new Cliente("Lima", 28);
var cliente4 = new Cliente("Fábio", 21);

var clientes = new RegistroDeClientes();

clientes.Adicionar(cliente1, cliente1.Idade);
clientes.Adicionar(cliente2, cliente2.Idade);
clientes.Adicionar(cliente3, cliente3.Idade);
clientes.Adicionar(cliente4, cliente4.Idade);

clientes.Listar();