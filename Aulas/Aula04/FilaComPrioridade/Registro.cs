using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilaComPrioridade
{
    public class Registro<T>
    {
        public PriorityQueue<T, int> itens {get; set;}
        
        public Registro()
        {
            itens = new PriorityQueue<T, int>();
        }

        public void Adicionar(T item, int prioridade)
        {
            itens.Enqueue(item, prioridade);
        }

        public void Listar()
        {
            /*while (itens.TryDequeue(out var item:T, out var prioridade:int))
            {
                Console.WriteLine(item.ToString());
            }*/
            while(itens.TryDequeue(out T item, out int prioridade))
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}