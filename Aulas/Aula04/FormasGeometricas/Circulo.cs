using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormasGeometricas
{
    public class Circulo : FormaGeometrica
    {
        public double Raio {get; set;}
        public Circulo(double raio)
        {
            Raio = raio;
        }
        public override double CalcularArea()
        {
            Area = Math.PI * Raio * Raio;
            return Area;
        }
        public override double CalcularPerimetro()
        {
            Perimetro = 2 * Math.PI * Raio;
            return Perimetro;
        }
    }
}