using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormasGeometricas
{
    public abstract class FormaGeometrica
    {
        public double Area {get; set;}
        public double Perimetro {get; set;}
        public abstract double CalcularArea();
        public abstract double CalcularPerimetro();
    }
}