﻿using System.Reflection;
using FormasGeometricas;

namespace FormasGeometricas;
internal class Program
{
    static void Main(string[] args)
    {
        var retangulo = new Retangulo(6,8);
        Console.WriteLine($"Área do retângulo: {retangulo.CalcularArea()}\nPerímetro do retângulo: {retangulo.CalcularPerimetro()}");
        var circulo = new Circulo(6.8);
        Console.WriteLine($"Área do Circulo: {circulo.CalcularArea()}\nPerímetro do Circulo: {circulo.CalcularPerimetro()}");
        var triangulo = new Triangulo(6,8);
        Console.WriteLine($"Área do Triângulo: {triangulo.CalcularArea()}\nPerímetro do Triângulo: {triangulo.CalcularPerimetro()}");
    }
}
