using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormasGeometricas
{
    public class Retangulo : FormaGeometrica
    {
        public double Lado1 {get; set;}
        public double Lado2 {get; set;}
        public Retangulo(double lado1, double lado2)
        {
            Lado1 = lado1;
            Lado2 = lado2;
        }
        public override double CalcularArea()
        {
            Area = Lado1 * Lado2;
            return Area;
        }
        public override double CalcularPerimetro()
        {
            Perimetro = Lado1 + Lado2;
            return Perimetro;
        }
    }
}