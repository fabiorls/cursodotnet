using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormasGeometricas
{
    public class Triangulo: FormaGeometrica
    {
        public double BaseTriangulo {get; set;}
        public double Altura {get; set;}
        public Triangulo(double baseTriangulo, double altura)
        {
            BaseTriangulo = baseTriangulo;
            Altura = altura;
        }
        public override double CalcularArea()
        {
            Area = BaseTriangulo * Altura / 2;
            return Area;
        }
        public override double CalcularPerimetro()
        {
            Perimetro = BaseTriangulo + Altura + (Math.Sqrt(Math.Pow(BaseTriangulo, 2)+Math.Pow(Altura, 2)));
            return Perimetro;
        }
    }
}