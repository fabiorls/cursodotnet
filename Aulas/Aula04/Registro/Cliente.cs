using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Registro
{
    public class Cliente
    {
        public string Nome {get; set;} = string.Empty;
        public int Idade {get; set;}
        public Cliente(string nome, int idade)
        {
            Nome = nome;
            Idade = idade;
        }
        public override string ToString()
        {
            return $"Cliente: {Nome}, de {Idade} anos de idade";
        }
    }
}