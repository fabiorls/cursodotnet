using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Registro
{
    public class Produto
    {
        public string Nome {get; set;} = string.Empty;
        public double Valor {get; set;}
        public Produto(string nome, double valor)
        {
            Nome = nome;
            Valor = valor;
        }
        public override string ToString()
        {
            return $"Produto: {Nome}, no valor de R$ {Valor}";
        }
    }
}