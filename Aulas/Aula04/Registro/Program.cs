﻿using Registro;

Console.WriteLine("----------Testando clientes----------");
var cliente1 = new Cliente("Fábio", 30);
var cliente2 = new Cliente("José", 25);
var cliente3 = new Cliente("Rodrigo", 38);

var clientes = new RegistroClientes();
clientes.Adicionar(cliente1);
clientes.Adicionar(cliente2);
clientes.Adicionar(cliente3);
clientes.Listar();
clientes.Remover(cliente2);
Console.WriteLine("Após remoção----------");
clientes.Listar();

Console.WriteLine("----------Testando produtos----------");
var produto1 = new Produto("Tv", 1800);
var produto2 = new Produto("Máquina de lavar", 1450);
var produto3 = new Produto("PS5", 3400);

var produtos = new RegistroProdutos();
produtos.Adicionar(produto1);
produtos.Adicionar(produto2);
produtos.Adicionar(produto3);
produtos.Listar();
produtos.Remover(produto2);
Console.WriteLine("Após remoção----------");
produtos.Listar();
