using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Registro
{
    public abstract class Registro<T>
    {
        public List<T> itens {get; set;}
        public Registro()
        {
            itens = new List<T>();
        }

        public void Adicionar(T item)
        {
            itens.Add(item);
        }
        public void Remover(T item)
        {
            itens.Remove(item);
        }
        public void Listar()
        {
            foreach(var value in itens)
                Console.WriteLine(value.ToString());
        }
    }
}