﻿public delegate int Operation(int x, int y);

public class Program
{
    public static void Main()
    {
        Operation operacao = Somar;
        Console.WriteLine(operacao(50, 5));
        operacao = Subtrair;
        Console.WriteLine(operacao(50, 5));
        operacao = Multiplicar;
        Console.WriteLine(operacao(50, 5));
        operacao = Dividir;
        Console.WriteLine(operacao(50, 5));
    }

    public static int Somar(int x, int y)
    {
        return x + y;
    }

    public static int Subtrair(int x, int y)
    {
        return x - y;
    }

    public static int Multiplicar(int x, int y)
    {
        return x * y;
    }

    public static int Dividir(int x, int y)
    {
        return x / y;
    }
}
