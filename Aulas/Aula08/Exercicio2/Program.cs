﻿using System.Runtime.CompilerServices;

public delegate int Operation(int x, int y);

public class Program
{
    public static void Main()
    {
        Operation op = Add;
        ExecuteOperation(delegate (int x, int y){return x + y;}, 50, 40);
    }

    static int Add(int x, int y) => x + y;
    static int Subtract(int x, int y) => x - y;
    static int Multiply(int x, int y) => x * y;
    static int Divide(int x, int y) => x / y;

    static void ExecuteOperation(Operation op, int x, int y)
    {
        int result = op(x, y);
        Console.WriteLine($"Resultado: {result}");
    }
}
