﻿using System.Runtime.CompilerServices;

public class Program
{
    public static void Main(string[] args)
    {
        List<string> lista = new List<string>{"Carro", "Moto", "Barco", "Avião"};
        
        lista.ForEach(nome => Console.WriteLine(nome.ToUpper()));
    }
}